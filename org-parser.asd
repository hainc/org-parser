(asdf:defsystem :org-parser
  :version      "0.1.0"
  :description  "description"
  :author       "Hai NGUYEN <haiuyeng@gmail.com>"
  :serial       t
  :license      "GNU AGPL, version 3"
  :components   ((:file "org-parser"))
  :depends-on   (#:uiop #:cl-ppcre))
