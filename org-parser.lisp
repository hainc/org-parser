(defpackage :org-parser
  (:use #:common-lisp)
  (:export
   #:filename->data
   #:univtime->timestamp
   #:timestamp->univtime))


(in-package org-parser)


;;; helpers
(defparameter *scanner-active-scheduled-or-deadline*
  ;; eval-print in emacs: `scheduled-regexp'
  (cl-ppcre:create-scanner "^\\s*(SCHEDULED|DEADLINE):\\s*<"))


(defparameter *scanner-properties-start*
  ;; eval-print in emacs: `property-start-re'
  (cl-ppcre:create-scanner "^[ 	]*:PROPERTIES:[ 	]*$"))


(defparameter *scanner-properties-end*
  ;; eval-print in emacs: `property-end-re'
  (cl-ppcre:create-scanner "^[ 	]*:END:[ 	]*$"))


(defparameter *scanner-properties-entry*
  ;; eval-print in emacs: `property-re'
  (cl-ppcre:create-scanner "^\\S*:[^:]+:[^:]+"))


(defparameter *day-of-week-3-letters* '("Mon" "Tue" "Wed" "Thu" "Fri" "Sat" "Sun"))

(defun int->day-of-week-short (int)
  (elt *day-of-week-3-letters* int))

(defun univtime->timestamp (univtime)
  "Used in debugging the parsing of timestamp into Common Lisp universal time value."
  (multiple-value-bind (sec min hr day mon yr dow dst-p tz)
      (decode-universal-time univtime)
    (declare (ignore sec dst-p tz))
    (format nil "~A-~2,'0d-~2,'0d ~A ~2,'0d:~2,'0d" yr mon day (int->day-of-week-short dow) hr min)))


(defun timestamp->univtime (timestamp-str)
  (let* (tt-comps
         tt-comps-third
         date-str time-str repeat-str
         (sec 0) min hour day month year)
    (setf tt-comps (uiop:split-string timestamp-str))
    ;; ensure tt-comps always have 4 elements, which matches with the number of arguments of the `destructuring-bind' below:
    (setq date-str (first tt-comps))
    (case (length tt-comps)
      (2 (setq time-str "00:01"))
      (3
       (setq tt-comps-third (third tt-comps))
       (cond
         ((cl-ppcre:scan "^[0-9]+:[0-9]+" tt-comps-third)
          (setq time-str tt-comps-third))
         (t (setq time-str "00:01" ; old regexp used: "^([+-]|[.][+-])[0-9]"
                  repeat-str tt-comps-third))))
      (4 (setq time-str (third tt-comps)
               repeat-str (fourth tt-comps)))
      (t (error "tt-comps should have 3 or 4 elements, but it is not: ~S!"
                tt-comps)))
    (destructuring-bind (year-str mon-str day-str)
        (uiop:split-string date-str :max 3 :separator "-")
      (setq year (parse-integer year-str)
            month (parse-integer mon-str)
            day (parse-integer day-str)))
    (destructuring-bind (hour-str min-str)
        (uiop:split-string time-str :max 2 :separator ":")
      (setq hour (parse-integer hour-str)
            min (parse-integer min-str)))
    (values (encode-universal-time sec min hour day month year)
            repeat-str)))

;;; parse line
;;;; heading
  (defparameter *scanner-heading*
    (cl-ppcre:create-scanner
     ;; DESIGN: changing this regex (especially the groups in the regex)
     ;; requires double-checking the functions that use this regex('s groups) as well
     ;; for any breakage there may be due to any grouping changes in the regexp.
     "^(\\*+)\\s*")
    )
  (defparameter *scanner-heading-tags*
    ;; DESIGN: could include grouping here but cl-ppcre only detects the last
    ;; matching group so better off using a regexp without grouping for fast
    ;; detection of the tag and then splitting the target string with the ":"
    ;; separator, as the following:
    "\\s+[:_0-9A-Za-z]+:$")

(defun heading->data (line%)
  "LINE%: the unmodified heading line to parse."
  (multiple-value-bind (wholeregexp-start wholeregexp-end
                        group1-match-index-array
                        group2-match-index-array)
      (cl-ppcre:scan *scanner-heading* line%)
    (and wholeregexp-end
         (let* ((level (elt group2-match-index-array 0))
                ;; a line without the leading stars
                (line-no-stars (subseq line% wholeregexp-end))
                (tags-phrase (cl-ppcre:scan-to-strings *scanner-heading-tags*
                                                       line-no-stars))
                (tag-alist (and (stringp tags-phrase)
                                (cdr ; the first element is an empty string due
                                     ; to the target string starting with the
                                     ; separator char #: ; therefore cdr is used
                                     ; to remove the initial empty string of the
                                     ; splitted string:
                                 (ppcre:split
                                  ":"
                                  (string-trim "     " tags-phrase)))))
                (line-heading (if tags-phrase (subseq line-no-stars 0
                                                      (- (length line-no-stars)
                                                         (length tags-phrase)))
                                  line-no-stars)))
           (list :heading line-heading
                 :level level
                 :tags tag-alist)))))

;;;; timestamp
(defparameter *scanner-timestamp-components* (ppcre:create-scanner ":\\s*")
  "Regexp to split the org timestamp line into its components")

(defparameter *scanner-timestamp-angled-brackets*
  (ppcre:create-scanner "^<([^>]+)>"))

(defun timestamp->components (timestamp-line)
  (let* ((line-split (cl-ppcre:split
                      *scanner-timestamp-components*
                      timestamp-line :limit 2))
         (timestamp-str (cl-ppcre:regex-replace
                         *scanner-timestamp-angled-brackets*
                                                (second line-split) "\\1")))
    (or (= (length line-split) 2)
        (error "Invalid argument: TIMESTAMP-LINE does not have a colon \":\"!"))
    (or (stringp timestamp-str)
        (error "Invalid argument: TIMESTAMP-LINE second group after the colon does not  have the form <...>!"))
    (multiple-value-bind (univ-time repeat-str)
        (timestamp->univtime timestamp-str)
          (values univ-time (first line-split) repeat-str))))

;;;; non-heading
(defun non-heading->data (line)
  "NON-HEADING: an a-list of lines below the heading line "
  (let* (result)
    (loop
      with state = 'right-after-heading
      with properties% = nil ; a line about the PROPERTIES of the heading
      with continuep = t
      with line% = nil
      with body% = nil
      while (and continuep
                 (null (zerop (length line)))
                 (setq line% (pop line)))
      do
      ;; first line could be PROPERTIES, DEADLINE, SCHEDULED, or a
      ;; BODY TEXT, in that order of importance:
         (case state
           (right-after-heading ; <<< so named to indicate the scanner just
                                        ; scanned a heading
            (cond
              ((cl-ppcre:scan *scanner-active-scheduled-or-deadline* line%)
               (multiple-value-bind (univtime type repeater)
                   (timestamp->components line%)
                 (setf (getf result :schedule) univtime
                       (getf result :schedule-type) type)
                 (and repeater
                      (setf (getf result :repeater) repeater))))
              ((cl-ppcre:scan *scanner-properties-start* line%)
               (unless (null properties%)
                 (warn "properties% is not null!")
                 (setq continuep nil))
               (setq state 'properties-start))
              (t ; wholesale adding all the remaining text into the current
                 ; node's :body, including the case of a scheduled lines or
                 ; deadline lines (mis)placed *after* some lines of the body
                                        ; text
               (push line% body%)
               (setq state 'body-start))))
           (properties-start
            (cond
              ((cl-ppcre:scan *scanner-properties-entry* line%)
               (push line% properties%))
              ((cl-ppcre:scan *scanner-properties-end* line%)
               (setf (getf result :properties) (nreverse properties%))
               (setq state 'body-start))
              (t (warn "Misplaced line in org-agenda format when in ~S; line: ~S!~&" state line%)
                 (setq continuep nil))))
           (t ; this must be the body, including the case of a scheduled line or deadline line placed *after* some lines of the body text
            (push line% body%)))
      finally (and body% (setf (getf result :body) (nreverse body%))))
    result))

;;;; general line
(defun filename->data-1 (infilename)
  "Stage-1 parsing of org agenda file INFILENAME."
  ;; org-agenda is used as the format for the timer because it can be processed by GNU Emacs and by Android OS's orgzly
  (let* (parse-tree)
    ;; parsing in multiple passes for easier debugging
    ;;
    ;; first pass: collecting heading and non-heading lines:
    (flet ((push-curnode-to-parse-tree% (non-heading% curnode%)
             ;; (format t "Reaching heading ~S~&" line%)
             (push (nreverse non-heading%) curnode%)
             (push :non-heading curnode%)
             (push curnode% parse-tree)))
      (with-open-file (instream infilename :direction :input)
        (loop
          with curnode% = nil
          with parsed-heading% = nil
          with properties% = nil
          with non-heading% = nil ; the collection of lines under an org-agenda heading
          for line% = nil
          ;; while new line is read or reaching end-of-file (ie. line% is nil) but curnode is not nil (ie the last parsing still accumulates in curnode%)
          while (setq line% (read-line instream nil))
          do (setq parsed-heading% (heading->data line%))
             (if parsed-heading%
                 (progn
                   (push-curnode-to-parse-tree% non-heading% curnode%)
                   (setq non-heading% nil
                         curnode% parsed-heading%))
                 (push line% non-heading%))
          finally
             (and non-heading% (push-curnode-to-parse-tree% non-heading% curnode%))
             ;; (format t "last curnode% ~S~&" curnode%)
             (setq parse-tree (nreverse parse-tree))
             ;; (break "parse-tree: ~S" parse-tree)
          )))
    parse-tree))

(defun filename->data (infilename)
  (let* ((parse-tree
           (filename->data-1 infilename)))
    (loop
      with result = nil
      for item in parse-tree
          for parsed-non-heading = (non-heading->data
                                     (getf item :non-heading))
      do (remf item :non-heading) ; replace the old :non-heading property of
                                  ; each item with new properties parsed from
                                  ; the old :non-heading property.      

         ;; add the modified item to the result:
         (push (append item parsed-non-heading) result)
      
      finally
         ;; recycle the parse-tree to store the new result:
         (setq parse-tree (nreverse result))
      )

    parse-tree))


(defun data->file (outfilename parse-tree)
  (with-open-file (outfile outfilename
                           :direction :output
                           :if-exists :supersede)
    ;; (break "parse-tree: ~S" parse-tree)
    (loop
      for node% in parse-tree
      for heading% = (getf node% :heading)
      for properties% = (getf node% :properties)
      for scheduled-univtime% = (getf node% :schedule)
      for scheduled% = (and scheduled-univtime%
                            (univtime->timestamp scheduled-univtime%))
      for schedule-type% = (getf node% :schedule-type)
      for repeater% = (getf node% :repeater)
      for body% = (getf node% :body)
      do
         (progn
           ;; (and properties% (error "properties: ~S!" node%))
           (and heading% (format outfile "~@[~A~]~C" heading% #\Newline))
           (and scheduled% (format outfile "~A: <~A~@[ ~A~]>~&"
                                   schedule-type% scheduled% repeater%))
           (and properties% (format outfile ":PROPERTIES:~C~@[~{~A~&~}~]:END:~C"
                                    #\Newline
                                    properties%
                                    #\Newline))
           (and body% (loop for line in body% do
                         (format outfile "~@[~A~]~C" line #\Newline))))))
  outfilename)

;;; tests
(defun data-diff-origfile (infilename tree stage error-handling)
  "Write the given TREE to a temporary file and compare it to the original INFILENAME for any differences."
  (let* ((outfilename
           (namestring (merge-pathnames "/tmp/" (file-namestring infilename)))))
    (format t "~C~CTesting parse-tree: infile: ~S, outfile: ~S, stage ~S~&%"
            #\Newline #\Newline infilename outfilename stage)
    (multiple-value-bind (outstr errstr exitcode)
        (file-diff infilename (data->file outfilename tree) t)
      (format t "~S~&~S~&~D" outstr errstr exitcode)
      ;; (break "exitcode: ~S, outstr: ~S, errstr: ~S" exitcode outstr errstr)
      (or (zerop exitcode)
          (format t "~%~% ERROR: non-zero exit code as above!"))
      exitcode)))

