<title>(pre-alpha stage, limit functionality) org-parser - parsing an org file into Lisp objects</title>

# Introduction

I could not find existing Common-Lisp-based parser for org files with a free or
open-source license so I roll one myself.

I am not going to make a full-blown parser yet, just enough parsing of those
fields of interest (identifying the headings, schedules, deadlines, properties,
and body) so that I can build a lightweight reminder for myself.

# License

AGPLv3
